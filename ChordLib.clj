(ns com.jalat.chordlib
  (:use viz))


;; Height and width of the keys on the keyboard. For white keys,
;; it is the width at the bottom of the key.
(def w-width 46)
(def w-height 280)
(def b-width 27)
(def b-height 180)

;; Define common colors.
(def b-color [20 20 20])
(def w-color [200 200 200])
;; Root color is used to hightlight the root key of the chord
;; pressed color is used to highlight the rest of the keys in the chord.
(def root-color [200 20 20])
(def pressed-color [80 100 250])
;; Colors for the display of the current chord and instructions for use.
(def chord-text-color [158 120 120])
(def instruction-color [240 240 240])

(defn member? [i seq]
  (some #{i} seq))

(def key-names [:C :C# :D :D# :E :F :F# :G :G# :A :A# :B])

(defn white? [k]
  (member? k [:C :D :E :F :G :A :B]))

(defn black? [k]
  (not (white? k)))

;; Define the width of the top end of the keys, this is needed to
;; place the black keys correctly. (eg. C and F has different width.)
(def key-top-widths {:C (- w-width (/ (* 2 b-width) 
				      3))
		     :C# b-width
		     :D (- w-width (/ (* 2 b-width) 
				      3))
		     :D# b-width
		     :E (- w-width (/ (* 2 b-width) 
				      3))
		     :F (- w-width (/ (* 3 b-width) 
				      4))
		     :F# b-width
		     :G (- w-width (/ (* 3 b-width) 
				      4))
		     :G# b-width
		     :A (- w-width (/ (* 3 b-width) 
				      4))
		     :A# b-width
		     :B (- w-width (/ (* 3 b-width) 
				      4))})

;; Define the offset of each key to be drawn based on widths defined
;; above. 

(def key-offset 
     (loop [key key-names
	    whites 0
	    offsets {}]
       (if (not key)
	 offsets
	 (recur (rest key)
		(if (white? (first key))
		  (inc whites)
		  whites)
		  (assoc offsets (first key)
			 (if (white? (first key))
			   (* whites w-width)
			   (reduce +
				   (map key-top-widths 
					(take-while #(not (= (first key) %))
						    key-names)))))))))

(def key-shapes
     (loop [key key-names
	    whites 0
	    offsets {}]
       (if (not key)
	 offsets
	 (recur (rest key)
		(if (white? (first key))
		  (inc whites)
		  whites)
		  (assoc offsets (first key)
			 (let* [x1 (reduce +
					  (map key-top-widths 
					       (take-while #(not (= (first key) %))
							   key-names)))
				x2 (+ x1 (key-top-widths (first key)))]
			       (concat [x1 b-height x1 0 x2 0 x2 b-height]
				       (if (white? (first key))
					  [(* (inc whites) w-width) b-height
					   (* (inc whites) w-width) w-height
					   (* whites w-width) w-height
					   (* whites w-width) b-height]))))))))

(defn draw-key [key color x-origin y-origin]
  "Note that white keys are drawn as a square, and the black keys 
are drawn on top to give the keys shape. The key argument is a keyboard-loc 
struct which has keys :color :octave and :key, octave and key is used to 
determine location of the key relative to x-origin and y-origin 
which is top left corner of the whole keyboard display"
  (let [octave (key :octave)
	key (key :key)
	x (+ (* 7 w-width octave) x-origin)
	y y-origin
	w (if (white? key)
	    w-width
	    b-width)
	h (if (white? key)
	    w-height
	    b-height)
	key-shape (apply concat (map #(list (+ x (first %))
					    (+ y (second %)))
				     (partition 2 (key-shapes key))))]
    (if (or (and (< y mouseY (+ y b-height))
		 (< (nth key-shape 0) mouseX (nth key-shape 4)))
	    (and (white? key)
		 (< (+ y b-height) mouseY (+ y w-height))
		 (< (nth key-shape 12) mouseX (nth key-shape 10))))
      (do (apply fill (map #(min 255 (+ % 80))
			   color))
	  (when mouseButton
	    (change-root-key key)))
      (apply fill color))
    (apply polygon key-shape)))

;; struct for a location on a keyboard, keeps track of location of a key

(defstruct keyboard-loc :key :octave)

;; A keyboard is a collection of keyboard-loc structs, we use three octaves.

(def keyboard
     (apply vector
	    (map (fn [[k o]] (struct keyboard-loc 
				     k o ))
		 (apply concat (map (fn [i]
					(map (fn [x] [x i])
					     key-names))
				    [0 1 2])))))

(def chords (array-map :maj {:notes [0 4 7] :name ""}
		       :min {:notes [0 3 7] :name "m"}
		       :seven {:notes   [0 4 7 10] :name "7"}
		       :m7 {:notes   [0 3 7 10] :name "m7"}
		       :sevenb5 {:notes [0 4 6 10] :name "7\u266D5"}
		       :m7b5 {:notes [0 3 6 10] :name "m7\u266D5"}
		       :maj7 {:notes [0 4 7 11] :name "maj7"}
		       :m6 {:notes   [0 3 7 9] :name "m6"}
		       :aug {:notes [0 4 8] :name "aug"}
		       :dim {:notes [0 3 6] :name "dim"}
		       :five {:notes    [0 7] :name "5"}
		       :sus {:notes [0 5 7] :name "sus"}
		       :sus2 {:notes [0 2 7] :name "sus2"}
		       :six {:notes   [0 4 7 9] :name "6"}
		       :add2 {:notes [0 2 4 7] :name "(add2)"}
		       :sevensus {:notes [0 5 7 10] :name "7sus"}
		       :madd2 {:notes [0 2 3 7] :name "m(add2)"}
		       :mmaj7 {:notes [0 3 7 11] :name "m(maj7)"}
		       :dim7 {:notes [0 3 6 9] :name "dim7"}
		       :maj7s11 {:notes [0 4 7 11 18] :name "maj7\u266F11"}
		       :maj9 {:notes [0 4 7 11 14] :name "maj9"}
		       :sevens9 {:notes  [0 4 7 10 15] :name "7\u266F9"}
		       :nine {:notes    [0 4 7 10 14] :name "9"}
		       :eleven{:notes   [0 7 10 14 17] :name "11"}
		       :thirteen {:notes   [0 4 7 10 14 21] :name "13"}
		       :m9 {:notes   [0 3 7 10 14] :name "m9"}
		       :m11 {:notes  [0 3 7 10 14 17] :name "m11"}))

;; Current chord displayed, default start chord is a C major.
;; The data in the seq is root, chord, inversion
(def current-chord (ref [0 :maj 0]))

(defn invert [chord num]
  "Changes the chord based on the inversion level given as an argument"
  (loop [ch chord
	 inv []
	 i num]
    (if (not ch)
      (reverse inv)
      (recur (rest ch)
	     (cons (if (< 0 i)
		     (+ 12 (first ch))
		     (first ch))
		   inv)
	     (dec i)))))

(defn print-chord [root name x y]
  "prints a chord at a given position"
  (text (str (subs (str (nth key-names (rem root 12))) 1) name) x y))

(defn print-current-chord []
  "Print the current chord displayed."
  (let [key ((nth keyboard (first @current-chord)) :key)
	name ((chords (second @current-chord)) :name)]
    (apply stroke chord-text-color)
    (text-alignment 'LEFT)
    (font "Times" 'PLAIN 100)
    (text (str (subs (str key) 1) name) 20 100)
    (font "Times" 'PLAIN 50)
    (text (nth ["Root position" "First inversion" "Second inversion" "Third inversion"]
	       (nth @current-chord 2)) 20 160)))

(defn change-root-key [key]
  "Changes the chord from eg. minor 7th to 9th. Keeps the root and 
inversion settings the same"
  (ref-set current-chord [(count (take-while #(not (= key %))
					     key-names))
			  (second @current-chord)
			  (nth @current-chord 2)]))

(defn move-chord [dir]
  "Changes the root of the current chord, does not change type of chord
or inversion"
  (ref-set current-chord [(if (= dir :up)
			    (rem (inc (first @current-chord)) 12)
			    (rem (+ (- (count keyboard) 1)
				    (first @current-chord)) 12))
			  (second @current-chord)
			  (nth @current-chord 2)]))

(defn change-chord-type [name]
  "Changes the chord from eg. minor 7th to 9th. Keeps the root and 
inversion settings the same"
  (ref-set current-chord [(first @current-chord)
			  name
			  (nth @current-chord 2)]))


(defn change-inversion [num]
  "Changes the inversion of the current chord, does not change the root
or type of chord."
    (ref-set current-chord [(first @current-chord)
			    (second @current-chord)
			    num]))

(def commands 
     {:down   {:fn #(move-chord :down) :chord nil}
      :up     {:fn #(move-chord :up) :chord nil}
      :left   {:fn #(move-chord :down) :chord nil}
      :right  {:fn #(move-chord :up) :chord nil}
      :0      {:fn #(change-inversion 0)  :chord nil}
      :1      {:fn #(change-inversion 1)  :chord nil}
      :2      {:fn #(change-inversion 2)  :chord nil}
      :3      {:fn #(change-inversion 3)  :chord nil}
      :q      {:fn #(change-chord-type :maj) :chord  :maj}
      :a      {:fn #(change-chord-type :min) :chord  :min}
      :w      {:fn #(change-chord-type :seven) :chord  :seven}
      :s      {:fn #(change-chord-type :m7)  :chord  :m7}
      :e      {:fn #(change-chord-type :maj7) :chord  :maj7}
      :d      {:fn #(change-chord-type :m7b5) :chord  :m7b5}
      :r      {:fn #(change-chord-type :six) :chord  :six}
      :f      {:fn #(change-chord-type :m6)  :chord  :m6}
      :z      {:fn #(change-chord-type :dim)  :chord  :dim}
      :g      {:fn #(change-chord-type :five) :chord  :five}
      :t      {:fn #(change-chord-type :aug) :chord  :aug}
      :c      {:fn #(change-chord-type :mmaj7) :chord  :mmaj7}
      :v      {:fn #(change-chord-type :sevenb5) :chord  :sevenb5}
      :b      {:fn #(change-chord-type :sus) :chord  :sus}
      :n      {:fn #(change-chord-type :sus2)  :chord  :sus2}
      :y      {:fn #(change-chord-type :add2) :chord  :add2}
      :m      {:fn #(change-chord-type :sevensus) :chord  :sevensus}
      :h      {:fn #(change-chord-type :madd2) :chord  :madd2}
      :l      {:fn #(change-chord-type :maj7s11) :chord  :maj7s11}
      :comma      {:fn #(change-chord-type :maj9) :chord  :maj9}
      :o      {:fn #(change-chord-type :sevens9) :chord  :sevens9}
      :u      {:fn #(change-chord-type :nine) :chord  :nine}
      :i      {:fn #(change-chord-type :eleven) :chord  :eleven}
      :p      {:fn #(change-chord-type :thirteen) :chord  :thirteen}
      :j      {:fn #(change-chord-type :m9) :chord  :m9}
      :k      {:fn #(change-chord-type :m11) :chord  :m11}
      :x      {:fn #(change-chord-type :dim7) :chord :dim7}})

(defn draw-keyboard [x-origin y-origin]
  "draws the whole keyboard, white keys first, then black so the
black keys cover up the whites and give them the correct shape"
  (let [chord-notes (map #(+ (first @current-chord)
			     %)
			 (invert ((chords (second @current-chord)) :notes) 
				 (nth @current-chord 2)))]
    (loop [key keyboard
	   i 0]
      (when (white? ((first key) :key))
	(draw-key (first key) 
		  (if (= (first chord-notes) i) 
		    root-color 
		    (if (member? i chord-notes)
		      pressed-color
		      w-color)) 
		  x-origin y-origin))
      (when (rest key)
	(recur (rest key) (inc i))))
    (loop [key keyboard
	   i 0]
      (when (black? ((first key) :key))
	(draw-key (first key)
		  (if (= (first chord-notes) i) 
		    root-color 
		    (if (member? i chord-notes)
		      pressed-color
		      b-color))
		  x-origin y-origin))
      (when (rest key)
	(recur (rest key) (inc i))))))

(defn print-chord-commands []
  "Prints out instructions for how to use the program."
  (font "Helvetica" 'PLAIN 20)
  (apply stroke instruction-color)
  (text "Use arrow keys to change root of chord" 520 50)
  (text "To change inversion, use number keys:" 520 80)
  (text "0 - root postition, 1 - 1st inversion, 2 - 2nd inversion" 520 105)
  (text "To change chord, use letters as shown below:" 520 165)
  (text-alignment 'CENTER)
  (loop [c "qazwsxedcrfvtgbyhnujmik,ol.p"
	 x 50
	 y 245
	 i 0]
    (when c
      (let [command (commands (keyword (str (if (= \, (first c)) "comma"
						(if (= \. (first c)) "period"
						    (first c))))))]
	(when command
	  (if (and (< -40 (- mouseX x) 40)
		   (< -20 (- mouseY (+ y (* i 55))) 20))
	    (do (font "Helvetica" 'BOLD 23)
		(when mouseButton
		  (change-chord-type (:chord command))))
	    (font "Helvetica" 'PLAIN 20))
	  (text (str (first c))
		x 
		(+ y (* i 55)))
	  (print-chord (first @current-chord) 
		       (:name (chords (:chord command)))
		       x
		       (+ y 20 (* i 55))))
	(recur (rest c)
	       (+ x 30)
	       y
	       (rem (inc i) 3))))))

(defn ChordLib-main [& args]
  (def piano (launch 1005 700))
  (once piano 
	(framerate 60)
	(smooth true))
  (draw piano 
	(dosync 
	 (background 50)
	 (draw-keyboard 20 (- height 300))
	 (print-current-chord)
	 (print-chord-commands)))
  (on-key-press piano
		(when (commands keyPressed)
		  (dosync ((:fn (commands keyPressed)))))))

